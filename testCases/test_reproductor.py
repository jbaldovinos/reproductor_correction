import unittest
import time
from selenium import webdriver
from pageObjects.HomePage import ReproductorHomePage
from pageObjects.ResultPage import BandcampResultPage
from pageObjects.ReproduccionPage import Reproduccion


class Test_Reproductor(unittest.TestCase):
    mombreArtista = input("Escribe el nombre de un artista  ")
    baseURL = "https://bandcamp.com/"
    driver = webdriver.Chrome('C:\\Users\\jbaldovinos\\Desktop\\drivers\\chromedriver.exe')

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver.get(cls.baseURL)
        cls.driver.maximize_window()

    # Test the search with wordSearch value
    def test_search(self):
        reproductorHomePage = ReproductorHomePage(self.driver)
        reproductorHomePage.sendInput(self.mombreArtista)
        reproductorHomePage.clickSearch()
        self.assertEqual("Search: " + self.mombreArtista + " | Bandcamp", self.driver.title,
                         "You are not in the search results")

    # Test that the reslusts matches with the search name
    def test_nombreCancion(self):
        reproductorResultPage = BandcampResultPage(self.driver)
        author = reproductorResultPage.validarBusqueda()
        self.assertEqual(self.mombreArtista, author.lower(), "Los resultados corresponden con la busqueda iniciada: ")


    # Test select one option to play
    def test_reproductor(self):
        time.sleep(4)
        reproducirMusica = BandcampResultPage(self.driver)
        reproducir = reproducirMusica.reproducir()
        self.assertEqual(reproducir, "si", "Se reproducira la canción")

    #Test that the song is playing
    def test_salida_audio(self):
        reproduccion = Reproduccion.clickReproducir()
        validacion = Reproduccion.validarReproduccion()
        self.assertEqual(validacion, 1, "Error al reproducir")

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.quit()
        print("The browser is closed")
