import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class Reproduccion():

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 15)
        self.boton_reproducir_xpath = "//div[@class='playbutton']"
        self.boton_reproduciendo = "//div[@class='playbutton playing']"

    def clickReproducir(self):
        try:
            self.wait.until(expected_conditions.visibility_of_element_located((By.XPATH, self.boton_reproducir_xpath)))
            self.driver.find_element_by_xpath(self.boton_reproducir_xpath).click()
        except:
            print("Error al buscar el elemento")

    def validarReproduccion(self):
        auxiliar = 0
        try:
            time.sleep(5)
            if expected_conditions.visibility_of_element_located((By.XPATH, self.boton_reproduciendo)):
                auxiliar = 1
                time.sleep(90)
            return auxiliar
        except:
            print("No se esta reproduciendo la canción")

