from sqlite3.dbapi2 import Time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait


class BandcampResultPage:

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 15)
        self.cancion_xpath = "//li[1]//div[1]//div[2]//a[1]"


    def validarBusqueda(self):
        try:
            self.wait.until(expected_conditions.visibility_of_element_located((By.XPATH, self.cancion_xpath)))
            author = self.driver.find_element_by_xpath(self.cancion_xpath).text
            self.driver.implicitly_wait(4)
        except:
            print("Elemento on localizable")
        return author

    #This method select the first option of the result list
    def reproducir(self):
        reproducir = input("Quieres reproducir canción: si/no")
        try:
            self.wait.until(expected_conditions.visibility_of_element_located((By.XPATH, self.cancion_xpath)))
            if reproducir.lower() == "si":
                self.cancion_xpath.click()
                return reproducir
            else:
                print("Intente ingresando otra busqueda")
        except:
            print("Uno o más elementos no se pudieron localizar")
