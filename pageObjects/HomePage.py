from sqlite3.dbapi2 import Time
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait


class ReproductorHomePage():
    searchbox_name = "q"
    searchlist_Xpath = "//ul[@class='result-items']//li[]"
    cancion_Xpath = "//li[2]//div[1]//div[2]"

    def __init__(self, driver):
        #self.searchlist_Xpath = "//h3[@class='LC20lb DKV0Md']"
        self.driver = driver

    # This function get the word form the console and send it to google search box
    def sendInput(self, mombreArtista):
        try:
            self.driver.find_element_by_name(self.searchbox_name).send_keys(mombreArtista)
        except:
            print("The word is not on the search box")

    # This function enter the value to begin with the search
    def clickSearch(self):
        try:
            self.driver.find_element_by_name(self.searchbox_name).send_keys(Keys.ENTER)
        except:
            print("Search incomplete")

    # This function prints the result list
    def resultList(self, mombreArtista):
        try:
            wait = WebDriverWait(self.driver, 40)
            self.results = wait.until(
                expected_conditions.presence_of_all_elements_located((By.XPATH, self.searchlist_Xpath)))
            data = []
            for item in self.results:
                data.append(item.text)

        except:
            print("La lista de resultados no es la correcta. "
                  "Intente con otro nombre, álbum o canción")

